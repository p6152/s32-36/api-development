const Course = require('./../models/Courses')



// CREATE COURSES - (WORKING AS INTENDED)
module.exports.createCourse = async (reqBody) => {
	const { courseName, description, price, } = reqBody

	const newCourse = new Course({
		courseName: courseName,
		description: description,
		price: price
	})
	return await  newCourse.save().then(result => {
		if(result){
			return `${courseName} course has been added`
		} else {
			if(result == null){
				return false
			}
		}
	})
}

// GET ALL COURSES - (WORKING AS INTENDED)
module.exports.getAllCourses = async (qActive) => {
	console.log(qActive)
	if(qActive){
		console.log(`if`)
		return await Course.find({isOffered: true}).then(result => result)
	}else{
		return await Course.find().then(result => result)
	}
}

// // GET ALL ACTIVE COURSE -  (ERROR ON POSTMAN)
module.exports.activeCourses = async () => {

	return await Course.find({isOffered:true}).then(result => result)
}

// RETRIEVE SPECIFIC COURSE - (WORKING AS INTENDED)
module.exports.getCourse = async (id) => {

	return await Course.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Course not found`}
			} else {
				return err
			}
		}
	})
}

// UPDATE COURSE - ONLY ADMIN CAN UPDATE - (WORKING AS INTENDED)
module.exports.updateCourse = async (courseId, reqBody) => {

	return await Course.findByIdAndUpdate(courseId, {$set: reqBody}, {new:true}).then(result => result)
}

// ARCHIVE COURSE - ONLY ADMIN CAN ARCHIVE - (WORKING AS INTENDED)
module.exports.archive = async (courseId) => {

	return await Course.findByIdAndUpdate(courseId, {$set: {isOffered: false}}, {new:true}).then(result => result)
}

// UNARCHIVE COURSE - ONLY ADMIN CAN AN UNARCHIVE - (WORKING AS INTENDED)
module.exports.unArchive = async (courseId) => {

	return await Course.findByIdAndUpdate(courseId, {$set: {isOffered: true}}, {new:true}).then(result => result)
}



// DELETE COURSE - (WORKING AS INTENDED)
module.exports.deleteCourse = async (reqBody) => {
	const {courseName} = reqBody

	return await Course.findOneAndDelete({courseName: courseName}).then((result, err) => result ? true : err)
}


