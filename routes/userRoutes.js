const express = require('express');
const router = express.Router();

// Import units of functions from userController module
const {
	register,
	getAllUsers,
	checkEmail,
	login,
	profile,
	updatePw,
	adminStatusToTrue,
	adminStatusToFalse,
	deleteUser,
	enroll
	
} = require('./../controllers/userControllers');

const {verify, decode, verifyAdmin} = require('./../auth');

//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//REGISTER A USER 
router.post('/register', async (req, res) => {
	// console.log(req.body)	//user object

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})


//CHECK IF EMAIL ALREADY EXISTS
router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})



//LOGIN THE USER
	// authentication
router.post('/login', async (req, res) => {
	// console.log(req.body)
	try{
		await login(req.body).then( result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		await profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//Create a route to update user information, make sure the route is secured with token
 // UPDATE USER INFO
router.put(`/update`, verify, async (req, res) => {
	// console.log(decode(req.headers.authorization).id)
	const userId = decode(req.headers.authorization).id
	try{
		await update(userId, req.body).then( result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//Create a route to update user's password, make sure the  route is secured with token
 //UPDATE USER PASSWORD

router.put(`/update-password`, verify, async(req, res) => {

	console.log( decode(req.headers.authorization).id )
	console.log(req.body.password)

	const userId = decode(req.headers.authorization).id
	try{
		await updatePw(userId, req.body.password).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

 
// UPDATE isAdmin status to "TRUE" 
	// route is secured with token
		// Only admin can update user's isAdmin status
// router.patch(`/isAdmin`, verify, async (req, res) =>{

// 	// console.log(req.body)
// 	// console.log(decode(req.headers.authorization).isAdmin)
// 	const admin = decode(req.headers.authorization).isAdmin
// 	try{
// 		 if(admin == true){
// 			await adminStatus(req.body).then(result => res.send(result))
// 		}else{
// 			res.send(`You are not authorized!`)
// 		}

// 	}catch(err){
// 		res.status(500).json(err)
// 	}
// })


// OR

router.patch(`/isAdmin`, verifyAdmin, async (req, res) =>{

	try{
			await adminStatusToTrue(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//UPDATE isAdmin status to "FALSE" 
	// route is secured with token
		// Only admin can update user's isAdmin status
router.patch(`/isUser`, verifyAdmin, async (req, res) =>{

	try{
			await adminStatusToFalse(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




// DELETE USER - Only admin can delete
router.delete(`/delete-user`, verifyAdmin, async (req, res) =>{

	try{
		await deleteUser(req.body).then(result => res.send(`User has been deleted`))
	}catch(err){
		res.status(500).json(err)
	}	
})

// ENROLL A USER

router.post(`/enroll`, verify, async (req, res) => {
	const user = decode(req.headers.authorization).isAdmin
	const data = {
		userId: decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	if(user == false){
		try{
			await enroll(data).then(result => res.send(result))
		}catch(err){
			res.status(500).json(err)
		}
	} else{
		res.send(`Only logged in user can enroll`)
	}
})



//Export the router module to be used in index.js file
module.exports = router;
