const express = require('express');
const router = express.Router();

const {
	createCourse, 
	getAllCourses, 
	getCourse, 
	updateCourse, 
	activeCourses, 
	archive, 
	unArchive, 
	deleteCourse} = require('./../controllers/courseControllers');

const {verifyAdmin, verify} = require('./../auth');


// CREATE COURSES - ONLY ADMIN CAN CREATE - (WORKING AS INTENDED)
router.post('/add-course', verifyAdmin, async (req, res) => {

	try{
		await createCourse(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



// GET ALL COURSES - BOTH ADMIN AND REGULAR USER CAN ACCESS - (WORKING AS INTENDED)
router.get(`/`, async (req, res) => {
	
	try{
		await getAllCourses().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// GET ALL ACTIVE COURSE - (ERROR ON POSTMAN)
router.get('/active', verify, async (req, res) => {
	try{
		await activeCourses().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



// RETRIEVE  A SPECIFIC COURSE - BOTH ADMIN AND REGULAR USER CAN ACCESS (WORKING AS INTENDED)
router.get('/:courseId', verify, async (req, res) => {
	
	// console.log(req.params.courseId)
	try{
		await getCourse(req.params.courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// UPDATE COURSE - ONLY ADMIN CAN UPDATE - (WORKING AS INTENDED)
router.put('/:courseId/update', verifyAdmin, async (req, res) => {
	try{
		await updateCourse(req.params.courseId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// ARCHIVE COURSE - ONLY ADMIN CAN ARCHIVE - (WORKING AS INTENDED)
router.patch('/:courseId/archive', verifyAdmin, async (req, res) => {
	try{
		await archive(req.params.courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// UNARCHIVE COURSE - ONLY ADMIN CAN AN UNARCHIVE - (WORKING AS INTENDED)
router.patch('/:courseId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unArchive(req.params.courseId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})





// DELETE COURSE - ONLY ADMIN CAN DELETE (WORKING AS INTENDED)
router.delete(`/delete-course`, verifyAdmin, async (req, res) =>{

	try{
		await deleteCourse(req.body).then(result => res.send(`${req.body.courseName} has been deleted`))
	}catch(err){
		res.status(500).json(err)
	}	
})





//Export the router module to be used in index.js file
module.exports = router;
